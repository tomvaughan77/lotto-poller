import cv2
import time
import requests

from datetime import datetime
from bs4 import BeautifulSoup
from luma.led_matrix.device import max7219
from luma.core.interface.serial import spi, noop
from luma.core.legacy import show_message
from luma.core.legacy.font import proportional, LCD_FONT


class MotionDetection():
    def __init__(self):
        self.video = cv2.VideoCapture(0)

    def __enter__(self):
        return self.video

    def __exit__(self, exc_type, exc_value, traceback):
        print('RELEASING VIDEO')
        try:
            cv2.destroyAllWindows()
        except Exception:
            pass
        self.video.release()

def capture_motion(reset_interval, scrape_interval):
    last_scrape = 0.0

    with MotionDetection() as video:
        while True:
            static_back = None
            last_movement = None
            t_end = time.time() + reset_interval

            while time.time() < t_end: 
                check, frame = video.read() 
                motion = False
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) 
                gray = cv2.GaussianBlur(gray, (21, 21), 0) 
            
                if static_back is None: 
                    static_back = gray 
                    continue
            
                diff_frame = cv2.absdiff(static_back, gray) 

                thresh_frame = cv2.threshold(diff_frame, 30, 255, cv2.THRESH_BINARY)[1] 
                thresh_frame = cv2.dilate(thresh_frame, None, iterations = 2) 
            
                cnts,_ = cv2.findContours(thresh_frame.copy(),  
                                cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE) 
            
                for contour in cnts: 
                    if cv2.contourArea(contour) < 10000: 
                        continue
                    motion = True

                if motion:
                    if time.time() - last_scrape > scrape_interval:
                        display_message(format_message(get_lotto_draw()))
                        last_scrape = time.time()


def get_lotto_draw():
    url='https://www.national-lottery.co.uk/results/lotto/draw-history'
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    table = soup.find(id='draw_history_lotto').find('ul', class_='table_row_group').find_all('li', class_='table_row_body')
    row = table[0].find_all('ul', class_='list_table_presentation')[0]

    raw_date = row.find('li', class_='table_cell_1').find('span', class_='table_cell_block')
    raw_numbers = row.find('li', class_='table_cell_3').find('span', class_='table_cell_block')
    raw_bonus = row.find('li', class_='table_cell_4').find('span', class_='table_cell_block')

    return {
        'date': datetime.strptime(cleanup_text_field(raw_date), '%a%d%b%Y').strftime('%a-%d-%b'),
        'numbers': cleanup_text_field(raw_numbers).split('-'),
        'bonus': cleanup_text_field(raw_bonus)
    }


def cleanup_text_field(field):
    return field.text.replace('\n', '').replace(' ', '')


def format_message(lotto):
    return f'Draw of {lotto["date"]}: {"-".join(lotto["numbers"])} Bonus-{lotto["bonus"]}'


def display_message(message):
    serial = spi(port=0, device=0, gpio=noop())
    device = max7219(serial, cascaded=4, block_orientation=90, rotate=0, blocks_arranged_in_reverse_order=True)
    
    for _ in range(3):
        print(f'Displaying: {message}')
        show_message(device, message, fill="white", font=proportional(LCD_FONT), scroll_delay=0.11)
        time.sleep(1)


capture_motion(5, 20)